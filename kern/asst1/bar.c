#include <types.h>
#include <lib.h>
#include <synch.h>
#include <test.h>
#include <thread.h>

#include "bar.h"
#include "bar_driver.h"


// #define PRINT_ON


/*
 * **********************************************************************
 * YOU ARE FREE TO CHANGE THIS FILE BELOW THIS POINT AS YOU SEE FIT
 *
 */

/* Declare any globals you need here (e.g. locks, etc...) */

// semaphores:
// **********************************************************************
/*
    These semaphores are initialized by 0, they are used to block customer
thread after order drink, and wake up the thread when order is served.
    They are used in order_drink() and serve_order()
*/
static struct semaphore *customer_state[NCUSTOMERS];
static struct semaphore *final_order;
/*
    This semaphore is initialized by 1, it is used to enforce mutual 
exclusion for accessing order_list[].
    They are used in order_drink() and take_order()
*/
static struct semaphore *order_mutex;
/*
    This semaphore is initialized by 1, it used to block bartender thread 
when no order is on order_list[]
    It wait before take_order() and signal after order_drink().
*/
static struct semaphore *order_pending;
/*
    This semaphore is initialized by 1, it is used to enforce mutual 
exclusion for accessing bottles[].
*/
static struct semaphore *bottle_mutex;
/*
    This semaphore is initialized by 1, it is used to enforce mutual 
exclusion for accessing to cid.
*/
static struct semaphore *cid_mutex;



#ifdef PRINT_ON
static struct semaphore *print_mutex;
#endif
// shared data
// **********************************************************************
/*
use a circular queue to maintain the list of orders, we have functions
to manipulate the queue:
    - enqueue()
    - dequeue()
which will be defind afterward
*/
static struct barorder* order_list[NCUSTOMERS];
static int read;
static int write;
// the next cid which will be assigned to a customer thread.
// a function getcid() will be defind afterwards
static int cid_number;
static struct barorder* cid_list[NCUSTOMERS];

// helper functions:
// **********************************************************************
/*
        dequeue and enqueue are used to manipulate order_list
*/
static void enqueue(struct barorder* order)
{
        order_list[write] = order;
        write = (write + 1) % NCUSTOMERS;
}

static struct barorder* dequeue(void)
{
        struct barorder* order = order_list[read];
        read = (read + 1) % NCUSTOMERS;
        return order;
}

// getcid() generate an unique number from 0 to NCUSTOMERS-1 for each customer
// is_new_customer() is just a helper function of getcid()
static int is_new_customer(struct barorder* order){
        int i;
        for (i = 0; i < cid_number; i++){
                if (order == cid_list[i]) {
                        return 0; //not new
                }
        }
        return 1; //can not find record, new customer

}

static int getcid(struct barorder* order){
        int current_cid;
        P(cid_mutex);
        if (is_new_customer(order) == 0) {
                // if this is the and existing customer's order
                // then just return his cid
                current_cid = order->cid;
        } else {
                // if this is the first order of a new customer
                // then assign cid to the order
                order->cid = cid_number;
                current_cid = order->cid;
                cid_list[cid_number] = order;
                cid_number++;
        }
        V(cid_mutex);
        return current_cid;
}


#ifdef PRINT_ON
// used for debug
static void print_order(struct barorder *order){
        kprintf("%p requested_bottles %d, %d, %d cid = %d flag = %d\n",
                order,
                order->requested_bottles[0],
                order->requested_bottles[1],
                order->requested_bottles[2],
                order->cid,
                order->go_home_flag
                // read,
                // write
        );
}
#endif
/*
 * **********************************************************************
 * FUNCTIONS EXECUTED BY CUSTOMER THREADS
 * **********************************************************************
 */

/*
 * order_drink()
 *
 * Takes one argument referring to the order to be filled. The
 * function makes the order available to staff threads and then blocks
 * until a bartender has filled the glass with the appropriate drinks.
 */

void order_drink(struct barorder *order)
{
        int cid = getcid(order);
        P(order_mutex);
        enqueue(order);
#ifdef PRINT_ON
        P(print_mutex);
        kprintf("order ");
        print_order(order);
        V(print_mutex);
#endif
        V(order_mutex);
        V(order_pending);

        if (order->go_home_flag == 0){
                P(customer_state[cid]);
        } else {
                P(final_order);
        }
}



/*
 * **********************************************************************
 * FUNCTIONS EXECUTED BY BARTENDER THREADS
 * **********************************************************************
 */

/*
 * take_order()
 *
 * This function waits for a new order to be submitted by
 * customers. When submitted, it returns a pointer to the order.
 *
 */

struct barorder *take_order(void)
{
        struct barorder *ret = NULL;
        P(order_pending);
        P(order_mutex);
        ret = dequeue();
#ifdef PRINT_ON
        P(print_mutex);
        kprintf("take ");
        print_order(ret);
        V(print_mutex);
#endif
        V(order_mutex);
        return ret;
}


/*
 * fill_order()
 *
 * This function takes an order provided by take_order and fills the
 * order using the mix() function to mix the drink.
 *
 * NOTE: IT NEEDS TO ENSURE THAT MIX HAS EXCLUSIVE ACCESS TO THE
 * REQUIRED BOTTLES (AND, IDEALLY, ONLY THE BOTTLES) IT NEEDS TO USE TO
 * FILL THE ORDER.
 */

void fill_order(struct barorder *order)
{

        /* add any sync primitives you need to ensure mutual exclusion
           holds as described */

        /* the call to mix must remain */
        P(bottle_mutex);
        mix(order);
        V(bottle_mutex);
}


/*
 * serve_order()
 *
 * Takes a filled order and makes it available to (unblocks) the
 * waiting customer.
 */

void serve_order(struct barorder *order)
{
        if (order->go_home_flag == 0){
                int cid = getcid(order);
                V(customer_state[cid]);
                // kprintf("serve ");
                // print_order(order);
        } else {
                V(final_order);
        }
}



/*
 * **********************************************************************
 * INITIALISATION AND CLEANUP FUNCTIONS
 * **********************************************************************
 */


/*
 * bar_open()
 *
 * Perform any initialisation you need prior to opening the bar to
 * bartenders and customers. Typically, allocation and initialisation of
 * synch primitive and variable.
 */

void bar_open(void)
{       
        read = 0;
        write = 0;
        cid_number = 0;

        /* initialize the semaphores */
        int i;
        for (i = 0; i < NCUSTOMERS; i++){
                customer_state[i] = sem_create("customer_state", 0);
                if (customer_state[i] == NULL) {
                        panic("sem customer_state create failed");
                }
        }

        order_mutex = sem_create("order_mutex", 1);
        if (order_mutex == NULL) {
                panic("sem order_mutex create failed");
        }

        order_pending = sem_create("order_pending", 0);
        if (order_pending == NULL) {
                panic("sem order_pending create failed");
        }

        bottle_mutex = sem_create("bottle_mutex", 1);
        if (bottle_mutex == NULL) {
                panic("sem bottle_mutex create failed");
        }

        cid_mutex = sem_create("cid_mutex", 1);
        if (cid_mutex == NULL) {
                panic("sem cid_mutex create failed");
        }

        final_order = sem_create("final_order", 0);
        if (final_order == NULL) {
                panic("sem final_order create failed");
        }

#ifdef PRINT_ON
        print_mutex = sem_create("print_mutex", 1);
        if (print_mutex == NULL) {
                panic("sem print_mutex create failed");
        }
#endif
}

/*
 * bar_close()
 *
 * Perform any cleanup after the bar has closed and everybody
 * has gone home.
 */

void bar_close(void)
{
        /* clean up the semaphore we allocated earlier */
        int i;
        for (i = 0; i < NCUSTOMERS; i++){
                sem_destroy(customer_state[i]);
        }
        sem_destroy(order_mutex);
        sem_destroy(order_pending);
        sem_destroy(bottle_mutex);
        sem_destroy(cid_mutex);
        sem_destroy(final_order);

#ifdef PRINT_ON
        sem_destroy(print_mutex);
#endif
}

