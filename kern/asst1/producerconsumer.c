/* This file will contain your solution. Modify it as you wish. */
#include <types.h>
#include <lib.h>    /* for kprintf */
#include <synch.h>  /* for P(), V(), sem_* */
#include "producerconsumer_driver.h"

/* Declare any variables you need here to keep track of and
   synchronise your bounded. A sample declaration of a buffer is shown
   below. You can change this if you choose another implementation. */

static struct pc_data buffer[BUFFER_SIZE];

// the global variables and function for implementing a circular queue
static int read = 0;
static int write = 0;

static void enqueue(struct pc_data item)
{
        buffer[write] = item;
        write = (write + 1) % BUFFER_SIZE;
}

static struct pc_data dequeue(void)
{
        struct pc_data item = buffer[read];
        read = (read + 1) % BUFFER_SIZE;
        return item;
}

// we use three semaphore to solving pro-con problem
// note that all this semaphore should be initialized in producerconsumer_startup()
static struct semaphore *mutex;
static struct semaphore *empty;
static struct semaphore *full;


/* consumer_receive() is called by a consumer to request more data. It
   should block on a sync primitive if no data is available in your
   buffer. */

struct pc_data consumer_receive(void)
{
        struct pc_data thedata;
        P(full);
        P(mutex);
        thedata = dequeue();
        V(mutex);
        V(empty);
        return thedata;
}

/* procucer_send() is called by a producer to store data in your
   bounded buffer. */

void producer_send(struct pc_data item)
{
        P(empty);
        P(mutex);
        enqueue(item);
        V(mutex);
        V(full);
}




/* Perform any initialisation (e.g. of global data) you need
   here. Note: You can panic if any allocation fails during setup */

void producerconsumer_startup(void)
{
        /* initialize the semaphore for pro-con problem */
        mutex = sem_create("mutex", 1);
        if (mutex == NULL) {
                panic("sem mutex create failed");
        }

        empty = sem_create("mutex", BUFFER_SIZE);
        if (empty == NULL) {
                panic("sem empty create failed");
        }

        full = sem_create("full", 0);
        if (full == NULL) {
                panic("sem full create failed");
        }
}

/* Perform any clean-up you need here */
void producerconsumer_shutdown(void)
{
        /* clean up the semaphore we allocated earlier */
        sem_destroy(mutex);
        sem_destroy(empty);
        sem_destroy(full);
}

