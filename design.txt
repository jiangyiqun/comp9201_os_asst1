# design

## how to identify different order

    in order to know which order is belong to which customer, we use 
getcid() to generate an unique number from 0 to NCUSTOMERS-1 for each
customer.
    which will be used in order_drink() and serve_order()

## shared data

- order_list[NCUSTOMERS] & read & write
    use a circular queue to maintain the list of orders, we have functions
to manipulate the queue, namely enqueue() and dequeue.
- bottles[NBOTTLES]
    which is already defined
- cid_number & cid_list[NCUSTOMERS]
    the next cid which will be assigned to a customer thread.
    the address of order of existed customers.
    this two are used in getcid()

## functions related to critical region

function    | manipulate critical region
------------|-----------------------------
order_drink | enqueue &order into order_list[], and block customer thread
take_order  | dequeue &order from order_list[]
fill_order  | change change bottles[] through mix()
serve_order | wake up customer thread
mix         | change bottles[]
go_home     | -

## semaphores:

### customer_state[NCUSTOMERS] & final_order

    These semaphores are initialized by 0, they are used to block customer
thread after order drink, and wake up the thread when order is served.
    final_order is used when all customers are left.
    They are used in order_drink() and serve_order()

### order_mutex
    
    This semaphore is initialized by 1, it is used to enforce mutual 
exclusion for accessing order_list[].
    They are used in order_drink() and take_order()

### order_pending

    This semaphore is initialized by 0, it used to block bartender thread 
when no order is on order_list[]
    It wait before take_order() and signal after order_drink().

### bottle_mutex

    This semaphore is initialized by 1, it is used to enforce mutual 
exclusion for accessing bottles[].

### cid_mutex

    This semaphore is initialized by 1, it is used to enforce mutual 
exclusion for accessing to cid.

